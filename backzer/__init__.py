from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from sqlalchemy.exc import IntegrityError
from configobj import ConfigObj
from backzer.auth.password import Password


config = ConfigObj('../config.ini')


# Set up the application
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['WTF_CSRF_SECRET_KEY'] = 'random key for form'
app.config['LDAP_PROVIDER_URL'] = 'ldap://ldap.testathon.net:389/'
app.config['LDAP_PROTOCOL_VERSION'] = 3
app.config['USER_EMAIL_SENDER_EMAIL'] = 'root@localhost'
db = SQLAlchemy(app)

app.secret_key = 'some_random_key'


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

from backzer.auth.views import auth
from backzer.admin.views import admin

app.register_blueprint(auth)
app.register_blueprint(admin)


from backzer.auth.models import User

db.create_all()




try:
    if bool(config['ADMIN']['enabled']):
        # Try to get the admin user
        admin = db.session.query(User).filter_by(id=1).first()
        if admin:
            admin.username = config['ADMIN']['local_admin_user']
            admin.password = Password.hash_password(config['ADMIN']['local_admin_password'])
            admin.auth_method = 'local'
            admin.is_admin = True
        else:
            admin = User(config['ADMIN']['local_admin_user'], Password.hash_password(config['ADMIN']['local_admin_password']), True, 'local')
            admin.id = 1
            db.session.add(admin)
        db.session.commit()
except IntegrityError:
    raise