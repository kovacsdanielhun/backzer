from backzer import app


class Backzer:
    def __init__(self):
        app.run(debug=True, host='0.0.0.0')


Backzer()
