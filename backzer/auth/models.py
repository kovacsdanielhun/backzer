from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import InputRequired
from backzer import db, config
from backzer.auth.auth_ldap import AuthLDAP
from backzer.auth.password import Password
import ldap



class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(100))
    auth_method = db.Column(db.String(100))
    is_admin = db.Column(db.Boolean())

    def __init__(self, username, password, is_admin=False, auth_method='local'):
        self.username = username
        self.password = password
        self.auth_method = auth_method
        self.is_admin = is_admin

    @staticmethod
    def create_user(username, password, is_admin, auth_method):
        if auth_method == 'ldap':
            user = User(username, None, is_admin, 'ldap')
        elif auth_method == 'local':
            user = User(username, password, is_admin, 'local')
        db.session.add(user)
        db.session.commit()
        return user

    @staticmethod
    def try_login(username, password, auth_method='local'):
        if auth_method == 'local':

            query = db.session.query(User).filter(User.username.is_(username))
            result = query.first()
            if not result:
                return False

            if Password.verify_password(result.password, password):
                return True
            return False

        elif auth_method == 'ldap':
            auth_ldap = AuthLDAP(config['AUTHENTICATION']['ldap_url'],
                                 config['AUTHENTICATION']['ldap_attribute_name'],
                                 config['AUTHENTICATION']['ldap_base'])
            try:
                auth_ldap.check_user(username, password)
            except ldap.INVALID_CREDENTIALS:
                return False
            return True

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    @staticmethod
    def get_users():
        """
        Returns a list of registered users
        :return: list
        """
        query = db.session.query(User)
        return query.all()



class LoginForm(Form):
    username = TextField('Username', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])