from flask import request, render_template, flash, redirect, url_for, Blueprint, g
from flask_login import current_user, login_user, logout_user, login_required
from backzer import login_manager, db, config
from backzer.auth.models import User, LoginForm

auth = Blueprint('auth', __name__)


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@auth.before_request
def get_current_user():
    g.user = current_user


@auth.route('/')
@auth.route('/home')
def home():
    if not current_user.is_authenticated:
        flash('You need to be logged in.', 'danger')
        return redirect(url_for('auth.login'))
    return render_template('home.html')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        flash('You are already logged in.', 'warning')
        return redirect(url_for('auth.home'))

    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate():
        username = request.form.get('username')
        password = request.form.get('password')
        auth_method = request.form.get('authmethod')

        if not User.try_login(username, password, auth_method.lower()):
            flash('Invalid username or password. Please try again.', 'danger')
            return render_template('login.html', form=form)

        user = User.query.filter_by(username=username).first()

        if not user and config['AUTHENTICATION'].as_bool('ldap_auto_create_users'):
            user = User.create_user(username, password, False, 'ldap')
            flash('Your user has been automatically created. Welcome.', 'success')
            login_user(user)
        else:
            flash('You have successfully logged in.', 'success')
            login_user(user)
        return redirect(url_for('auth.home'))

    if form.errors:
        flash(form.errors, 'danger')

    return render_template('login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.home'))