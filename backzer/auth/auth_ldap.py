import ldap


class AuthLDAP:
    def __init__(self, ldap_url, attribute_name, ldap_base):
        self.attribute_name = attribute_name
        self.search_dn = ldap_base
        self.conn = ldap.initialize(ldap_url)

    def check_user(self, attribute_value, password):
        who = self.attribute_name + '=' + attribute_value + ',' + self.search_dn
        self.conn.simple_bind_s(who, password)
