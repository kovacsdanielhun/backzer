from backzer import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    hostname = db.Column(db.String(100), nullable=False, unique=True)
    ip = db.Column(db.String(100), nullable=False)
    os = db.Column(db.String, primary_key=True)
    status = db.Column(db.String(100))
    enabled = db.Column(db.Boolean, default=True)

    def __init__(self, hostname, ip, os, status, enabled=True):
        self.hostname = hostname
        self.ip = ip
        self.os = os
        self.status = status
        self.enabled = enabled
