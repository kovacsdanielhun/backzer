from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, AnyOf


class NewUserForm(Form):
    username = StringField('Username', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])
    auth_method = PasswordField('Password', [AnyOf(['local', 'ldap'])])
    is_admin = PasswordField('Password', [AnyOf(['true', 'false'])])