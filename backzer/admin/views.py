from flask import  render_template, Blueprint
from flask import flash, redirect, url_for
from flask_login import current_user, login_required
from backzer import app, db
from backzer.auth.models import User
from backzer.admin.models import NewUserForm
from flask import request
from backzer.auth.password import Password
from sqlalchemy.exc import IntegrityError


admin = Blueprint('admin', __name__)

@admin.route('/admin')
@login_required
def base():
    if current_user.is_authenticated:
        if not current_user.is_admin:
            flash('User error, please contact site administrator.', 'danger')
            return redirect(url_for('auth.home'))

        return render_template('admin/base.html')

@admin.route('/admin/configuration')
@login_required
def configuration():
    """
    This view is for displaying the current configuration in non-editable format. The admin must edit the configuration
    in the config.ini file and restart the application!
    :return:
    """
    if current_user.is_authenticated:
        if not current_user.is_admin:
            flash('User error, please contact site administrator.', 'danger')
            return redirect(url_for('auth.home'))

        import pprint
        pprint.pprint(app.config)

        return render_template('admin/configuration.html', config=app.config)


@admin.route('/admin/user', methods=['GET', 'POST'])
@login_required
def user():
    if current_user.is_authenticated:
        if not current_user.is_admin:
            flash('User error, please contact site administrator.', 'danger')
            return redirect(url_for('auth.home'))

        form = NewUserForm(request.form)

        if request.method == 'POST' and form.validate():
            username = request.form.get('username')
            password = Password.hash_password(request.form.get('password'))
            auth_method = request.form.get('auth_method')

            if request.form.get('is_admin') == "true":
                is_admin = True
            else:
                is_admin = False

            try:
                User.create_user(username, password, bool(is_admin), auth_method)
                flash("New user has been added successfully!", 'success')
            except IntegrityError:
                db.session.rollback()
                flash("This username has already been taken, please choose a different one!", 'danger')

        if form.errors:
            flash(form.errors, 'danger')

        return render_template('admin/new_user.html', form=form)

@admin.route('/admin/users')
@login_required
def users():
    if current_user.is_authenticated:
        if not current_user.is_admin:
            flash('User error, please contact site administrator.', 'danger')
            return redirect(url_for('auth.home'))

        users = User.get_users()
        return render_template('admin/list_users.html', users=users)