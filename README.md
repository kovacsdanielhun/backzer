# backzer

Extensible and modern alternative of RANCID and Oxidized network backup systems written in Python3.

This project is in a pre-pre-pre-alpha stage now, so please don't try to use it..


# Requirements
## V1
- [ ] Oxidized API compatibility
- [ ] built-in lightweight dashboard
  - [ ] Refresh devices list
  - [ ] Device list with basic actions
    - [ ] Get latest configuration
    - [ ] Initiate configuration
- [ ] Device list source
  - [ ] Static file
- [ ] Configuration retrieve system
  - [ ] Modular, per platform commands to be executed 
    - [ ] via SSH
    - [ ] async with Celery
- [ ] Modular output system
  - [ ] txt
- [ ] Very basic user management
  - [ ] Admin panel
    - [x] Add users manually
    - [x] List users
    - [x] Show configuration
  - [x] Authentication
    - [x] LDAP
     - [ ] Choose to create users automatically or just manually  
    - [x] Local

## V2

- [ ] Device list source
  - [ ] API
- [ ] Modular, per platform commands to be executed 
  - [ ] via API, Telnet
- [ ] Modular output system
  - [ ] git repository
  
## Installation
### Centos7
sudo yum install https://centos7.iuscommunity.org/ius-release.rpm
sudo yum install python36u python36u-devel python36u-pip openldap-devel

Set up python packages:
sudo yum groupinstall "Development Tools"

# Sources
## CSV
## API
